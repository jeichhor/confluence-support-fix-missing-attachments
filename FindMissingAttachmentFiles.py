import codecs
import os
import shutil
import sys
from distutils.dir_util import copy_tree, remove_tree

print "Confluence Attachment Fixing Tool v1.5 - Dave Norton [Atlassian]"
print "Please ensure Confluence has been shut down, and you have a full backup of the home directory"
print ""

# Grab our attachment path
attachmentPath = raw_input('Where is your Confluence attachments directory: ')

# Clear out the files we're going to write to so we have a clean slate:
open('MissingFiles.txt', 'w').close()
open('PathsToCheck.txt', 'w').close()
open('FoundFiles.txt', 'w').close()

# If there's a trailing slash, get rid of it:
if attachmentPath.endswith('/'):
    attachmentPath = attachmentPath[:-1]

# Check the attachments directory is acceptable:
if not (os.path.isdir(os.path.join(attachmentPath, 'ver003'))):
    print '[Error] - The directory "%s" is not a valid attachments directory' % attachmentPath
    sys.exit(1)

# Make sure our required files are present:
for required_file in ['Attachments.txt']:
    if not os.path.exists(required_file):
        print '[Error] Could not find %s' % required_file
        sys.exit(1)


# This is our mod function
def mod(the_id):
    the_id = int(the_id);
    firstFolder = int(the_id % 250);
    secondFolder = int(((the_id - (the_id % 1000)) / 1000) % 250);
    return os.path.join(str(firstFolder), str(secondFolder), str(the_id));


# a Function for spitting a list to disk
def listtodisk(thelist, filename):
    with open(filename, 'w') as F:
        for item in thelist:
            F.write("%s\n" % item)
        F.close()
    print ""


# a function for zapping the Byte Order Marker
# based on http://stackoverflow.com/questions/8898294/convert-utf-8-with-bom-to-utf-8-with-no-bom-in-python
# as well as https://github.com/terencez127/BomSweeper
def bomzap(filename):
    BUFSIZE = 4096
    BOMLEN = len(codecs.BOM_UTF8)

    with open(filename, 'r+b') as fp:
        chunk = fp.read(BUFSIZE)
        if chunk.startswith(codecs.BOM_UTF8):
            i = 0
            chunk = chunk[BOMLEN:]
            while chunk:
                fp.seek(i)
                fp.write(chunk)
                i += len(chunk)
                fp.seek(BOMLEN, os.SEEK_CUR)
                chunk = fp.read(BUFSIZE)
            fp.seek(-BOMLEN, os.SEEK_CUR)
            fp.truncate()


def last2(path):
    r = path.split(os.sep)
    return os.path.join( r[-2],r[-1] )


# First, open up "files.txt" for reading, and generate a file path from the attachments in it:
pathsToCheck = []

# Zap the BOM (if any) in Attachments.txt
print 'Cleaning up the Byte Order Marker in Attachments.txt if any'
bomzap('Attachments.txt');

print 'Generating paths based on IDs in Attachments.txt'
with open('Attachments.txt') as f:
    for line in f:
        ids = line.strip().split('\t')
        if len(ids) != 4:
            print '[ERROR] Line %s does not conform to the spec - {spaceid}\\t{pageid}}\\t{contentid}}i\\t{version}' % (line,)
            continue

        # Create a path based on each id:
        space = ids[0]
        page = ids[1]
        content = ids[2]
        version = ids[3]

        # Whip up a path:
        pathsToCheck.append(
            os.path.join(attachmentPath, 'ver003', mod(space), mod(page), content, version))

# Output paths to check to a file:
print 'Writing paths to check to PathsToCheck.txt'
listtodisk(pathsToCheck, 'PathsToCheck.txt')

# Next, checking to see if those files exist on disk:
print 'Performing a check on the files listed in PathsToCheck.txt'
missingFiles = []
with open('PathsToCheck.txt') as f:
    for line in f:
        line = line.strip()
        if not os.path.exists(line):
            missingFiles.append(line)
print 'There are %s missing files' % len(missingFiles)

# Save the missing files to disk:
if len(missingFiles) > 0:
    print 'Writing the missing file paths to MissingFiles.txt'
    listtodisk(missingFiles, 'MissingFiles.txt')

# Should we attempt to fix things?
if len(missingFiles) > 0:
    tocontinuefind = raw_input(
        'This tool can attempt to find and recover the missing files. Would you like to do this now? (y/n): ')

    if tocontinuefind.lower() == 'y':
        print('Building a hash of your attachments directory. This will take a while - stand by!')

        possiblePaths = {}
        foundfiles = []

        for root, dirs, files in os.walk(attachmentPath):
            if len(dirs) > 0:
                # We don't want this- continue:
                continue

            for f in files:
                f = os.path.join(root,f)
                possiblePaths[ last2(f) ] = f

        print('Hash table complete - {0} paths in total. Searching for lost attachments....').format(len(possiblePaths))

        # Now that we have our hashtable, let's lookup the missing attachments
        for path in missingFiles:
            attachmentID = last2(path) 
            if attachmentID in possiblePaths:
                foundfiles.append('%s|%s' % (possiblePaths[attachmentID], path))

        # Spit that out to a file:
        listtodisk(foundfiles, 'FoundFiles.txt')

        print('Found {0} lost attachments - saved them to FoundFiles.txt. Attempting recovery...').format(
            len(foundfiles))
        recoveryCount = 0

        for index, comboPath in enumerate(foundfiles):
            # Get our source and destination:
            source = comboPath.split('|')[0]
            destination = comboPath.split('|')[1]
            destdir = os.path.dirname(destination)

            # Attempt to make the destination if it does not exist:
            if not os.path.exists(destdir):
                os.makedirs(destdir)

            # Success flag:
            success = True

            try:
                shutil.copy(os.path.normpath(source), os.path.normpath(destination))
            except shutil.Error as e:
                success = False
                print('File not copied. Error: {0}').format(e)

            except OSError as e:
                success = False
                print('Directory not copied. Error: {0}').format(e)
            else:
                # Try and remove the source directory if we copied it successfully:
                #not implemented for filebased logic
                #remove_tree(source)

                # Increment the recovery counter
                recoveryCount = recoveryCount + 1

            # Define our successText
            successText = 'Success!' if success else 'Failure :('
            # print ('File {0} of {1}: Moving {2} to {3} ... {4}').format((index + 1), len(foundfiles), source, destination, successText)
            print ('Recovering File {0} of {1}: Moving ... {2}').format((index + 1), len(foundfiles), successText)

        # We've completed!
        print('The process has completed. {0} attachments of {1} have been successfully recovered.').format(
            recoveryCount,
            len(foundfiles))

else:
    print 'Your attachments are healthy and in line with the database definitions!'
