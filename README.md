## Confluence Support - Fix Missing Attachments
FindMissingAttachments.py is an attachment fixing tool, designed to fix and repair attachments that are missing on disk in Confluence. Since attachment IDs do not change during page moves and are used as a part of the directory structure, we can determine where an attachment should be from the database, and where it currently is from the filesystem.

FindMissingAttachmentfiles.py in difference to the above tries to restore single files, in cases where files from earlier versions are found in different space directories.

## Disclaimer
Ensure Confluence has been shut down, and you have a full file system backup
Do not run this against production - run it against a test server first!

### Instructions FindMissingAttachments.py - Confluence 5.7 and above:
Run the following SQL against your system:

    SELECT spaceid, pageid, contentid
    FROM content
    WHERE contenttype = 'ATTACHMENT'
    AND prevver IS NULL
    and spaceid IS NOT NULL
    
You can alternatively limit to a specific space by name...

    SELECT c.spaceid, c.pageid, c.contentid
    FROM content c
    JOIN SPACES s ON s.spaceid = c.spaceid
    WHERE c.contenttype = 'ATTACHMENT'
    AND c.prevver IS NULL
    AND c.spaceid IS NOT NULL
    AND s.spacename = 'Demonstration Space'
    
... or by space key:

    SELECT c.spaceid, c.pageid, c.contentid
    FROM content c
    JOIN SPACES s ON s.spaceid = c.spaceid
    WHERE c.contenttype = 'ATTACHMENT'
    AND c.prevver IS NULL
    AND c.spaceid IS NOT NULL
    AND s.spacekey = 'DS'
      
Once you have the results, export them in a tab delimited format, with no headers and save it in this directory as `Attachments.txt`. The script will use this as a basis to build a series of paths to check. The file name is case sensitive!

Next up, run `python FindMissingAttachments.py` and follow the prompts.


### Instructions FindMissingAttachmentFiles.py - Confluence 5.7 and above:
Run the following SQL against your system:

    SELECT spaceid, pageid, coalesce(prevver,contentid), version
    FROM content WHERE contenttype = 'ATTACHMENT'
    AND spaceid IS NOT NULL

You can alternatively limit to a specific space by name...

    SELECT spaceid, pageid, coalesce(prevver,contentid), version
    FROM content c
    JOIN SPACES s ON s.spaceid = c.spaceid
    WHERE c.contenttype = 'ATTACHMENT'
    AND c.prevver IS NULL
    AND c.spaceid IS NOT NULL
    AND s.spacename = 'Demonstration Space'

... or by space key:

    SELECT spaceid, pageid, coalesce(prevver,contentid), version
    FROM content c
    JOIN SPACES s ON s.spaceid = c.spaceid
    WHERE c.contenttype = 'ATTACHMENT'
    AND c.prevver IS NULL
    AND c.spaceid IS NOT NULL
    AND s.spacekey = 'DS'

Once you have the results, export them in a tab delimited format, with no headers and save it in this directory as `Attachments.txt`. The script will use this as a basis to build a series of paths to check. The file name is case sensitive!

Next up, run `python FindMissingAttachmentFiles.py` and follow the prompts.



Problems? Attach the `*.txt` files in this directory to your support ticket, and your engineer will be able to help you out.
